﻿<%@ Page Language="C#" AutoEventWireup="true" CodeBehind="page.aspx.cs" Inherits="WebUI.web.page" %>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><%=article.Rows[0]["title"].ToString() %>-桂电求索网</title>
    <link href="../css/style.css" type="text/css" rel="stylesheet">
    <link href="../css/style4.css" type="text/css" rel="stylesheet">
    <script src="../js/iscroll.js" type="text/javascript"></script>
</head>

<body>
    <div id="winb" style="width: 1345px; margin: auto">
        <script>
            //var win;
            //win = window.screen.width;
            //if(win > 1300 && win < 1400)
            //{
            //    winb.style.width = "100%";
            //}
        </script>

        <header style="height: 260px">
            <div id="header_search">
                <div style="width: 80px; height: 19px; margin-top: 30px">
                    <p>站内搜索：</p>
                </div>
                <input type="text" class="search" style="margin-left: 71px;">
                <input type="button" class="submit">
            </div>
            <img id="top_img" src="../images/top_img.jpg" style="width: 100%; height: 201px">
        </header>

        <div id="header_nav" style="position: relative; top: -220px">
            <ul>
                <li><a href="login.html">登录</a></li>
                <li><a href="register.html">注册</a></li>
                <li><a href="#">设为主页</a></li>
                <li><a href="#">加入收藏</a></li>
            </ul>
        </div>

        <div id="aside_nav">
            <ul onmouseover="fun()" id="navigation">
                <li><a href="index.html">返回首页</a></li>
                <li><a href="explore.html">求索简介</a></li>
                <li><a href="web_dev.aspx">网站开发</a></li>
                <li><a href="employment.aspx">就业指导</a></li>
                <li><a href="study.aspx">学习资源</a></li>
                <li><a href="guet_news.aspx">桂电新闻</a></li>
                <li><a href="movie.aspx">电影推荐</a></li>
                <li><a href="public.html">我要发表</a></li>
            </ul>
        </div>
        <script>
            var i = 0;
            function fun() {
                if (i == 0) {
                    var acli = document.getElementsByClassName("active");
                    acli[0].style.background = "inherit";
                    this.onmouseout = function () {
                        acli[0].style.background = "url(../images/aside_nav_li_bg.png) no-repeat";
                    }
                }
                else if (i == 1) {
                    i = 0;
                }
            }

            var type = "<%=article.Rows[0]["type"].ToString() %>";
            var sel_type = document.getElementById("navigation").getElementsByTagName("li");
            switch(type)
            {
                case "网站开发":
                    sel_type[2].className = "active";
                    sel_type[2].setAttribute('onmouseover', "i = 1");
                    top_img.src = "../images/web_top.jpg";
                    break;
                case "就业指导":
                    sel_type[3].className = "active";
                    sel_type[3].setAttribute('onmouseover', "i = 1");
                    top_img.src = "../images/employment_top.jpg";
                    break;
                case "学习资源":
                    sel_type[4].className = "active";
                    sel_type[4].setAttribute('onmouseover', "i = 1");
                    top_img.src = "../images/study_top.jpg";
                    break;
                case "桂电新闻":
                    sel_type[5].className = "active";
                    sel_type[5].setAttribute('onmouseover', "i = 1");
                    top_img.src = "../images/news_top.jpg";
                    break;
                case "电影推荐":
                    sel_type[6].className = "active";
                    sel_type[6].setAttribute('onmouseover', "i = 1");
                    top_img.src = "../images/film_top.jpg";
                    break;
            }
        </script>

        <div id="content">
            <h1><%=article.Rows[0]["title"].ToString() %></h1>
            <div class="middle">
                <h3>作者：<%=article.Rows[0]["publisher"].ToString() %></h3>
                <h3>日期：<%=Convert.ToDateTime(article.Rows[0]["set_time"]).ToString("yyyy-MM-dd") %></h3>
                <img src="../images/solid.png">
            </div>

            <div id="massage_container_a">
                <%=article.Rows[0]["main"].ToString() %>
            </div>
            <div id="massage_container">
                <h2>点击评论</h2>
                <img src="../images/solid.png">
                <h1><a href="">阅读下一篇>></a></h1>
                <p>留言</p>
                <div id="cemment">
                    <div class="cemment1">
                        <table cellspacing="0" cellpadding="0">
                            <thead>
                                <td style="text-align: right;">张三：</td>
                                <td style="width: 620px">我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是</td>
                                <td style="width: 50px">回复</td>
                                <td style="width: 70px">2016-1-1</td>
                            </thead>
                        </table>
                    </div>
                    <div class="cemment2_2">
                        <table cellspacing="0" cellpadding="0">
                            <thead>
                                <td>
                                    <div class="p_right">
                                        <h4>张三：</h4>
                                        <div>
                                </td>
                                <td style="width: 620px">
                                    <div class="p_left">
                                        <h4>我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是</h4>
                                    </div>
                                </td>
                                <td style="width: 50px">
                                    <div class="p_bottom">
                                        <h4>回复</h4>
                                        <div>
                                </td>
                                <td style="width: 70px">
                                    <div class="p_bottom">
                                        <h4>2016-1-1</h4>
                                    </div>
                                </td>
                            </thead>
                        </table>
                    </div>
                    <div class="cemment1">
                        <table cellspacing="0" cellpadding="0">
                            <thead>
                                <td style="text-align: right;">张三：</td>
                                <td style="width: 620px">我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是</td>
                                <td style="width: 50px">回复</td>
                                <td style="width: 70px">2016-1-1</td>
                            </thead>
                        </table>
                    </div>
                    <div class="cemment31">
                        <div class="cemment3_3">
                            <table cellspacing="0" cellpadding="0">
                                <thead>
                                    <td style="text-align: center;">张三 回复 王五:</td>
                                    <td style="width: 539px">我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是</td>
                                    <td style="width: 51px">回复</td>
                                    <td style="width: 70px">2016-1-1</td>
                                </thead>
                            </table>
                        </div>
                        <div class="cemment3_2">
                            <table cellspacing="0" cellpadding="0">
                                <thead>
                                    <td>
                                        <div class="h_right">
                                            <h4>张三 回复 王五:</h4>
                                            <div>
                                    </td>
                                    <td style="width: 539px">
                                        <div class="h_left">
                                            <h4>我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是</h4>
                                        </div>
                                    </td>
                                    <td style="width: 51px">
                                        <div class="h_bottom">
                                            <h4>回复</h4>
                                            <div>
                                    </td>
                                    <td style="width: 70px">
                                        <div class="h_bottom">
                                            <h4>2016-1-1</h4>
                                        </div>
                                    </td>
                                </thead>
                            </table>
                        </div>
                        <div class="cemment3_3">
                            <table cellspacing="0" cellpadding="0">
                                <thead>
                                    <td style="text-align: center;">张三 回复 王五:</td>
                                    <td style="width: 539px">我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是</td>
                                    <td style="width: 51px">回复</td>
                                    <td style="width: 70px">2016-1-1</td>
                                </thead>
                            </table>
                        </div>
                    </div>
                    <div class="cemment2">
                        <table cellspacing="0" cellpadding="0">
                            <thead>
                                <td style="text-align: right;">张三：</td>
                                <td style="width: 620px">我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是</td>
                                <td style="width: 50px">回复</td>
                                <td style="width: 70px">2016-1-1</td>
                            </thead>
                        </table>
                    </div>
                    <div class="cemment32">
                        <table cellspacing="0" cellpadding="0">
                            <thead>
                                <td style="text-align: center;">张三 回复 王五:</td>
                                <td style="width: 539px">我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是</td>
                                <td style="width: 51px">回复</td>
                                <td style="width: 70px">2016-1-1</td>
                            </thead>
                        </table>
                    </div>
                    <div class="cemment1">
                        <table cellspacing="0" cellpadding="0">
                            <thead>
                                <td style="text-align: right;">张三：</td>
                                <td style="width: 620px">我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是</td>
                                <td style="width: 50px">回复</td>
                                <td style="width: 70px">2016-1-1</td>
                            </thead>
                        </table>
                    </div>
                    <div class="cemment2">
                        <table cellspacing="0" cellpadding="0">
                            <thead>
                                <td style="text-align: right;">张三：</td>
                                <td style="width: 620px">我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是</td>
                                <td style="width: 50px">回复</td>
                                <td style="width: 70px">2016-1-1</td>
                            </thead>
                        </table>
                    </div>
                    <div class="cemment1">
                        <table cellspacing="0" cellpadding="0">
                            <thead>
                                <td style="text-align: right;">张三：</td>
                                <td style="width: 620px">我们是我们是我们是我们是我们是我们是我们是我们是我们是我们是</td>
                                <td style="width: 50px">回复</td>
                                <td style="width: 70px">2016-1-1</td>
                            </thead>
                        </table>
                    </div>
                </div>
            </div>
            <div class="last">
                <p><a href="#">上一页 下一页</a></p>
                <input type="text" name="tex">
                <p class="p"><a href="#">发表留言</a></p>
            </div>

        </div>

        <footer>
            <img src="../images/link.png" class="link_logo">
            <ul>
                <li><a href="http://www.gliet.edu.cn">桂电首页</a></li>
                <li><a href="http://iit.guet.edu.cn/main/default.asp">信息科技学院</a></li>
                <li><a href="http://219.159.198.136/">桂电北海校区</a></li>
                <li><a href="http://www2.guet.edu.cn/gfs/">桂电国防生</a></li>
                <li><a href="http://www2.guet.edu.cn/cxlt/cxlt2008/index.asp">研究生创新论坛</a></li>
                <li><a href="http://news.gliet.edu.cn/">桂电在线</a></li>
                <li><a href="http://www.myclub2.com/">第二频道</a></li>
                <li><a href="http://bbs.gliet.edu.cn/bbs/">漓江夜话</a></li>
                <li><a href="http://sctc.guet.edu.cn/">创新在线</a></li>
            </ul>
        </footer>
    </div>
</body>
</html>
