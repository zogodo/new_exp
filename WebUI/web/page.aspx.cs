﻿using System;
using System.Data;

namespace WebUI.web
{
    public partial class page : System.Web.UI.Page
    {
        public DataTable article = new DataTable("");

        protected void Page_Load(object sender, EventArgs e)
        {
            string page_id = Request.QueryString["id"];

            article = DAL.PageServer.GetOnePage(Int32.Parse(page_id));
        }
    }
}