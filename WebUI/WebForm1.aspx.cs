﻿using Helper;
using Model;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace new_exp
{
    public partial class WebForm1 : System.Web.UI.Page
    {
        protected IList<User> admin { get; set; }
        protected string json { get; set; }

        protected void Page_Load(object sender, EventArgs e)
        {
            DataTable tb = SqlHelper.ExecuteDataTable("select * from [new_exp].[dbo].[user]");
            json = JsonHelper.ToJson(tb);
            admin = ModelConvertHelper<User>.ConvertToModel(tb);//ModelConvertHelper这个类也用了反射，泛型和泛型约束
        }
    }
}