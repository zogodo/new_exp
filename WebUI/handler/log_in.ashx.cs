﻿using BLL;
using System.Web;
using System.Web.SessionState;

namespace WebUI.handler
{
    /// <summary>
    /// log_in 的摘要说明
    /// </summary>
    public class log_in : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";

            string password = context.Request["password"];
            string username = context.Request["username"];

            int a = Login.Logoin(username, password); //a用来存储Login()的返回值

            if (a == 0)
            {
                context.Response.Write("登陆成功");
                context.Session["UserName"] = username;
                //context.Session["PassWord"] = password;
            }
            else if (a == 1)
            {
                context.Response.Write("用户不存在");
            }
            else if (a == 2)
            {
                context.Response.Write("密码错误");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}