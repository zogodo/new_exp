﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using BLL;

namespace WebUI.handler
{
    /// <summary>
    /// sign_up 的摘要说明
    /// </summary>
    public class sign_up : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";

            Model.SignUpPerson sp = new Model.SignUpPerson();

            sp.username = context.Request["username"];
            sp.password = context.Request["password"];
            sp.stuid = context.Request["stuid"];
            sp.name = context.Request["name"];
            sp.id = context.Request["id"]; //身份证后六位

            SignUp s = new SignUp(); //创建一个BLL里的SignUp对象

            int a = s.sign_up(sp); //a用来存储SignUp()的返回值，检测是否注册成功

            if (a == 0) //返回0表示注册成功
            {
                context.Response.Write("注册成功");
            }
            else if (a == 1) //返回1表示用户名已被注册
            {
                context.Response.Write("用户名已被注册");
            }
            else //不明错误
            {
                context.Response.Write("不明错误");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}