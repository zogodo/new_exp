﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.handler
{
    /// <summary>
    /// article 的摘要说明
    /// </summary>
    public class article : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";
            string articleId = context.Request["ArticleId"];//获取文章
            string type = context.Request["Type"];   //获取文章类型
            GetInformation GI = new GetInformation();
            string html = GI.GetArticle(articleId, "content", type);
            context.Response.Write(html);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}