﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.handler
{
    /// <summary>
    /// index 的摘要说明
    /// </summary>
    public class index : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";
            GetInformation get = new GetInformation();
            string html = get.IndexNews();
            context.Response.Write(html);
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}