﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.handler
{
    /// <summary>
    /// publish 的摘要说明
    /// </summary>
    public class publish : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";

            //验证登录

            Model.Page pa = new Model.Page();

            pa.title = context.Request["page_title"];
            pa.type = context.Request["page_type"];
            pa.publisher = context.Request["publisher"];
            pa.main = context.Request["page_main"];
            pa.main = pa.main.Replace("<.and.>", "&");
            pa.main = pa.main.Replace("<.shup.>", "#");

            //验证传回的字符串是否安全
            pa.main = Safe.StringSafeHtml(pa.main);
            pa.title = Safe.StringSafeHtml(pa.title);
            pa.type = Safe.StringSafeHtml(pa.type);
            pa.publisher = Safe.StringSafeHtml(pa.publisher);

            BLL.PulishPage pu = new BLL.PulishPage();

            if (pu.Pulish(pa) == 0)
            {
                context.Response.Write("发表成功");
            }
            else
            {
                context.Response.Write("发表失败");
            }
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}