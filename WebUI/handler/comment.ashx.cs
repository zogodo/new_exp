﻿using BLL;
using System;
using System.Web;
using System.Web.SessionState;

namespace WebUI.handler
{
    /// <summary>
    /// comment 的摘要说明
    /// </summary>
    public class comment : IHttpHandler, IRequiresSessionState
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";

            //检验登录
            CheckLogin.checkLogin(context);

            string user = (string)context.Session["username"];  //从session取到登陆人

            string action = context.Request["action"];  //action是从评论页面传回来

            //action=comment是评论,=reply是回复

            //发表评论
            if (action == "comment")
            {
                /* 评论人提交回来的数据 */
                //string UserName = context.Request["username"]; //评论人*
                string Text = context.Request["text"];         //评论内容*
                //string Time = context.Request["time"];         //时间*
                int PageId = Convert.ToInt32(context.Request["pageId"]);     //文章id
                int Id = Convert.ToInt32(context.Request["id"]);             //评论表id,表的id应该全部设置为自动增长
                /* 评论人提交回来的数据 */

                Model.Comment Mod_com = new Model.Comment();
                Mod_com.username = user;    //发表评论的人是登陆的人，没登陆不能发表评论
                Mod_com.text = Text;
                Mod_com.page_id = PageId;
                Mod_com.id = Id;

                AddComment add = new AddComment();
                int a = add.Add(Mod_com);
                if (a == 0)
                {
                    context.Response.Write("发表成功");
                }
                else
                    context.Response.Write("发表失败");
            }           //此处输出的值返回给评论人看


            //回复某人
            else if (action == "reply")
            {
                /* 回复人 */
                string To_UserName = context.Request["username"];    //收到回复的人从每一条评论中取
                int PageId = Convert.ToInt32(context.Request["pageId"]);
                int CommId = Convert.ToInt32(context.Request["comId"]);   //回复的ID
                int Id = Convert.ToInt32(context.Request["id"]);          //这个id是表的id，建议不要，设置成自动增长
                string Text = context.Request["text"];
                /* 回复人 */

                Model.Reply Mod_rep = new Model.Reply();
                Mod_rep.username = user;          //发表回复的人从session中取
                Mod_rep.to_username = To_UserName;
                Mod_rep.text = Text;
                Mod_rep.id = Id;
                
                Reply re = new Reply();
                int r = re.reply(Mod_rep);
                if (r == 0)
                {
                    context.Response.Write("回复成功");
                }
                else
                    context.Response.Write("回复失败");

            }



            //if (action == "add")
            //{

            //}
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}