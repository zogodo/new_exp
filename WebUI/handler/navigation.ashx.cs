﻿using BLL;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace WebUI.handler
{
    /// <summary>
    /// navigation 的摘要说明
    /// </summary>
    public class navigation : IHttpHandler
    {

        public void ProcessRequest(HttpContext context)
        {
            context.Response.ContentType = "text/html";
            string action = context.Request["Action"];
            int Paging = Convert.ToInt32(context.Request["Paging"]);//第Paging页，如第2页
            GetInformation GI = new GetInformation();//获取信息类
            string html = "";
            switch (action)
            {
                case "web":
                    html = GI.GetNews("web", "web-development", Paging);    //假如数据库表中有一些类型为web的新闻，则获取这种类型的新闻
                    context.Response.Write(html);
                    break;
                case "employment":
                    html = GI.GetNews("employment", "employment", Paging);
                    context.Response.Write(html);
                    break;
                case "study":
                    html = GI.GetNews("study", "study", Paging);
                    context.Response.Write(html);
                    break;
                case "news":
                    html = GI.GetNews("news", "guet_home", Paging);
                    context.Response.Write(html);
                    break;
                case "film":
                    html = GI.GetNews("film", "film", Paging);
                    context.Response.Write(html);
                    break;
                default:
                    GetInformation MsgBox = new GetInformation();
                    string str = MsgBox.MessageBox("action：" + action + "\n" + "action错误，请联系管理员");
                    context.Response.Write(str);
                    break;
            }


            //这些注释不要删除,如果swich语句有错则用这些
            //这些注释不要删除,如果swich语句有错则用这些
            //string article = context.Request["article"];
            //string html = "";
            //if (action == "web")
            //{
            //    GetInformation GI = new GetInformation();
            //    html = GI.GetNews("web");    //假如数据库表中有一些类型为web的新闻，则获取这种类型的新闻
            //    context.Response.Write(html);
            //}
            //else if (action == "employment")
            //{
            //    GetInformation GI = new GetInformation();
            //    html = GI.GetNews("employment");
            //    context.Response.Write(html);
            //}
            //else if (action == "study")
            //{
            //    GetInformation GI = new GetInformation();
            //    html = GI.GetNews("study");
            //    context.Response.Write(html);
            //}
            //else if (action == "news")
            //{
            //    GetInformation GI = new GetInformation();
            //    html = GI.GetNews("news");
            //    context.Response.Write(html);
            //}
            //else if (action == "film")
            //{
            //    GetInformation GI = new GetInformation();
            //    html = GI.GetNews("film");
            //    context.Response.Write(html);
            //}
            //else if (action == "" || action == null)        //这个 else if 要不要看情况
            //{
            //    GetInformation MsgBox = new GetInformation();
            //    string str = MsgBox.MessageBox("action为空，可能出错了，请联系管理员");
            //    context.Response.Write(str);
            //}
        }

        public bool IsReusable
        {
            get
            {
                return false;
            }
        }
    }
}