﻿namespace Model
{
    public class SignUpPerson
    {
        public string username {get; set;}
        public string password {get; set;}
        public string name {get; set;}
        public string stuid {get; set;}
        public string id { get; set; } //身份证后六位。
    }
}
