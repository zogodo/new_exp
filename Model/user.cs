﻿using System;

namespace Model
{
    public class User
    {
        public long id{ get; set; }
        public string username{ get; set; }
        public string password{ get; set; }
        public string name{ get; set; }
        public string stu_id{ get; set; }
        public int active{ get; set; }
        public DateTime sign_up_time{ get; set; }
        public DateTime last_login { get; set; }
    }
}
