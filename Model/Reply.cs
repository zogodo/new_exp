﻿using System;

namespace Model
{
    public class Reply
    {
        public long id { get; set; }
        public long comm_id { get; set; }       //评论的id
        public string username { get; set; }    //发回复的用户
        public string to_username { get; set; } //收回复的用户
        public string text { get; set; }        //回复的内容
        public DateTime time { get; set; }      //回复时间的
    }
}
