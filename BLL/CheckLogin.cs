﻿using System.Web;

namespace BLL
{
    public class CheckLogin
    {
        public HttpContext context;
        public CheckLogin(HttpContext context)
        {
            this.context = context;
        }

        public static void checkLogin(HttpContext context)
        {
            //检查当前是否登录
            if (context.Session["UserName"] == null)
            {
                context.Response.Write("没有登录");
            }
            else
            {
                context.Response.Write("已经登录");
            }
        }
    }
}
