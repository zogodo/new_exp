﻿namespace BLL
{
    public class Login
    {
        //用户登录的代码
        public static int Logoin(string username, string password)
        {
            DAL.User u = new DAL.User();

            if(u.CheckUsername(username) != 1)
            {
                return 1; //不存在此用户名
            }

            string p = u.GetPassword(username);

            if (p == password)
            {
                return 0; //密码正确
            }
            else
            {
                return 2; //密码错误
            }
        }
    }
}
