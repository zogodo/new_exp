﻿using Explore;
using System;
using System.Collections.Generic;
using System.Data;
using System.Linq;
using System.Text;

namespace BLL
{
    public class GetInformation
    {
        public string GetNews(string type,string htm,int page)  //获取新闻,加载分页，返回
        {
            DAL.GetInformation GT = new DAL.GetInformation();
            int count = GT.NewsCount("表名", type);//获得新闻的总条数

            //每页展示新闻数量为8
            int pageCount = (int)Math.Ceiling(count / 8.0);//pageCount是有多少页
            object[] pageData = new object[pageCount];
            for(int i = 0; i < pageCount; i++)
            {
                pageData[i] = new { Href = "../handler/navigation.ashx?Action=" + type + "&Paging=" + (i + 1), PageNum = (i + 1) };
            }
            int start = (page - 1) * 8 + 1;   //page是页码
            int end = page * 8;
            int upPage = page - 1;
            int downPage = page + 1;
            DataTable dt = GT.GetNews(type, start, end);
            var data = new { News = dt.Rows, paging = pageData, up = upPage, down = downPage };
            string html = CommonHelper.RenderHtml("../WebUI/html/" + htm + ".html", data);
            return html;
        }

        public string GetArticle(string articleId, string content, string type)
        {
            DAL.GetInformation GA = new DAL.GetInformation();
            DataTable dt = GA.GetArticle(articleId);
            DataTable table = GA.GetAllArticle(type);
            string NextId = "";
            for (int i = 0; i < table.Rows.Count; i++)
            {
                DataRow row = table.Rows[i];
                if (Convert.ToString(row["id"]) == articleId)
                {
                    row = table.Rows[i + 1];
                    NextId = Convert.ToString(row["id"]);
                    break;
                }
            }
            var data = new { Article = dt.Rows, next = NextId };
            string html = CommonHelper.RenderHtml("../WebUI/html/" + content + ".html", data);
            return html;
        }

        public string IndexNews()  //首页数据
        {
            DAL.GetInformation IN = new DAL.GetInformation();
            DataTable dt1 = IN.IndexNews("web",5);
            DataTable dt2 = IN.IndexNews("employment",5);
            DataTable dt3 = IN.IndexNews("study",6);
            DataTable dt4 = IN.IndexNews("news",6);
            DataTable dt5 = IN.IndexNews("film",6);
            var data = new { web = dt1, employment = dt2, study = dt3, news = dt4, film = dt5 };
            string html = CommonHelper.RenderHtml("../WebUI/html/index.html", data);
            return html;
        }


        public string MessageBox(string TxtMessage)  //弹出消息框()
        {
            string str;
            str = "<script language=javascript>alert('" + TxtMessage + "')</script>";
            return str;
        }
    }
}
