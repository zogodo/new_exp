﻿namespace BLL
{
    public class AddComment
    {
        //Comment co = new Comment();   //DAL.Comment

        public int Add(Model.Comment co)
        {
            DAL.Comment dal_Com = new DAL.Comment();
            int a = dal_Com.AddComment(co); //a用来存储AddComment()的返回值，检测评论是否成功
            return a;
        }
    }
}
