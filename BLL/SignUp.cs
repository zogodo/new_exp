﻿using DAL;

namespace BLL
{
    public class SignUp
    {
        User u = new User(); //新建一个DAL下的User对象
        CheckPeople c = new CheckPeople();

        public int sign_up(Model.SignUpPerson sp)
        {
            //用户注册

            int a = u.CheckUsername(sp.username); //a用来存储CheckUsername()的返回值，检测用户名是否可用
            int b = c.Check(sp);                 //b用来存储Check()的返回值，检测身份证后六位是否正确

            if (a == 0 && b == 0) //CheckUsername()返回 0 表示可用，Check()返回 0 表示身份证后六位正确
            {
                u.AddUser(sp);
                return 0; //注册成功，返回 0
            }
            else if (a == 1) //用户名已被注册
            {
                return 1; //CheckUsername()返回 1 表示用户名已被注册
            }
            else if (b == 1) //身份证后六位不正确
            {
                return 3;
            }
            else //不明错误
            {
                return 2; //不明错误，返回 2
            }
        }
    }
}
