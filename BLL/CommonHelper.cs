﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using NVelocity.App;
using NVelocity.Runtime;
using NVelocity;

namespace Explore
{
    public class CommonHelper        //这个模板引擎只能放在这里了，放在其他地方会有不明错误
    {
        /// <summary>
        /// 用data数据填充templateName模板，渲染生成html返回
        /// </summary>
        /// <param name="templateName"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static string RenderHtml(string templateName, object data)   //定义一个RenderHtml函数
        {
            VelocityEngine vltEngine = new VelocityEngine();
            vltEngine.SetProperty(RuntimeConstants.RESOURCE_LOADER, "file");
            vltEngine.SetProperty(RuntimeConstants.FILE_RESOURCE_LOADER_PATH, System.Web.Hosting.HostingEnvironment.MapPath("~/WebUI/html"));//模板文件所在的文件夹
            vltEngine.Init();

            VelocityContext vltContext = new VelocityContext();
            vltContext.Put("Data", data);//设置参数，在模板中可以通过$data来引用
            //将data的数据复制给Data，Data和data是同级别的，必须具有相同的属性
            Template vltTemplate = vltEngine.GetTemplate(templateName);        //Template(模板)
            System.IO.StringWriter vltWriter = new System.IO.StringWriter();
            vltTemplate.Merge(vltContext, vltWriter);

            string html = vltWriter.GetStringBuilder().ToString();
            return html;             //将获取到的html文件内容返回给RenderHtml函数
        }

        //public static string NullOrFull(string Judge)
        //{
        //    string data;
        //    if (string.IsNullOrEmpty(Judge))
        //    {
        //        data = "必填信息";
        //        return data;
        //    }
        //    else
        //    {
        //        data = "";
        //        return data;
        //    }
        //}
    }
}