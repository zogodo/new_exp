﻿using Helper;
using System;
using System.Data.SqlClient;

namespace DAL
{
    public class Reply
    {
        public int AddReply(Model.Reply re)
        {
            //给某条评论发一条回复
            DateTime now = new DateTime();
            SqlHelper.ExecuteNonQuery(@"inser into reply (comm_id, username, to_username, text, time) 
                                      value(@comm_id, @username, @@to_username @text, @time)",
                                      new SqlParameter("@comm_id", re.comm_id),
                                      new SqlParameter("@username", re.username),
                                      new SqlParameter("@to_username", re.to_username),
                                      new SqlParameter("@text", re.text),
                                      new SqlParameter("@time", now));
            return 0;
        }
    }
}
