﻿using Helper;
using System;
using System.Data.SqlClient;

namespace DAL
{
    public class User
    {
        public void AddUser(Model.SignUpPerson sp)
        {
            //添加一个用户
            DateTime now = new DateTime();
            now = DateTime.Now;
            SqlHelper.ExecuteNonQuery(@"insert into [user] (username, password, stu_id, sign_up_time, last_login) 
                                      values(@username, @password, @stuid, @sign_up_time, @last_login)",
                                      new SqlParameter("@username", sp.username),
                                      new SqlParameter("@password", sp.password),
                                      new SqlParameter("@stuid", sp.stuid),
                                      new SqlParameter("@sign_up_time", now),
                                      new SqlParameter("@last_login", now));
        }

        public string GetPassword(string username)
        {
            //获取某用户的密码（加密的）
            string password;
            password = SqlHelper.ExecuteScalar("select password from [user] where username = @username",
                                               new SqlParameter("@username", username)).ToString();
            return password;
        }

        public int CheckUsername(string username)
        {
            //查询数据库中是否有此用户名
            string count;
            count = SqlHelper.ExecuteScalar("select count(1) from [user] where username = @username",
                                               new SqlParameter("@username", username)).ToString();
            return Int32.Parse(count);
        }
    }
}
