﻿using Helper;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class PageServer
    {
        //关于文章的所有数据操作

        public static void AddPage(Model.Page pa)
        {
            //添加一篇文章
            DateTime now = new DateTime();
            now = DateTime.Now;
            SqlHelper.ExecuteNonQuery(@"insert into page (title, type, publisher, main, set_time, edit_time) 
                                      values(@title, @type, @publisher, @main, @set_time, @edit_time)",
                                      new SqlParameter("@title", pa.title),
                                      new SqlParameter("@type", pa.type),
                                      new SqlParameter("@publisher", pa.publisher),
                                      new SqlParameter("@main", pa.main),
                                      new SqlParameter("@set_time", now),
                                      new SqlParameter("@edit_time", now));
        }

        public static DataTable GetTypeOfPage(string type)
        {
            //获取某类型的文章
            DataTable pagesOfType = new DataTable();
            pagesOfType = SqlHelper.ExecuteDataTable("select id, title, set_time, publisher from page where type=@type",
                                      new SqlParameter("@type", type));
            return pagesOfType;
        }

        public static DataTable GetGoodPage()
        {
            //获取热门文章
            DataTable goodPages = new DataTable();
            goodPages = SqlHelper.ExecuteDataTable("select * from comment.....");
            return goodPages;
        }

        public static DataTable GetOnePage(int id)
        {
            //获取某一篇
            DataTable pages = new DataTable();
            pages = SqlHelper.ExecuteDataTable("select * from page where id=@id",
                                      new SqlParameter("@id", id));
            return pages;
        }
    }
}
