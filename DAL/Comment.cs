﻿using Helper;
using System;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class Comment
    {
        //关于评论和回复的所有数据操作

        public int AddComment(Model.Comment co)
        {
            //给某篇文章添加一条评论，并插入数据表
            DateTime now = new DateTime();
            SqlHelper.ExecuteNonQuery(@"inser into comment (page_id, username, text, set_time, rank)
                                      value(@page_id, @username, @text, @set_time)",
                                      new SqlParameter("@page_id", co.page_id),
                                      new SqlParameter("@username", co.username),
                                      new SqlParameter("@text", co.text),
                                      new SqlParameter("@set_time", now));
            return 0;
        }

        public DataTable GetComment(int page_id)
        {
            //获取某篇文章的所有评论
            DataTable all_comment = new DataTable();
            all_comment = SqlHelper.ExecuteDataTable("select * from comment.....");
            return all_comment;

        }

        public DataTable toUser(string username)
        {
            //获取某个用户收到的所有回复
            DataTable all_comment = new DataTable();
            all_comment = SqlHelper.ExecuteDataTable("select * from comment.....");
            return all_comment;
        }

        public DataTable userTo(string username)
        {
            //获取某个用户所有发表的评论和回复
            DataTable all_comment = new DataTable();
            all_comment = SqlHelper.ExecuteDataTable("select * from comment.....");
            return all_comment;
        }
    }
}
